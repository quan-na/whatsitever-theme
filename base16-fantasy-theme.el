;; base16-fantasy-theme.el -- A base16 colorscheme with fantasy named colors

;;; Commentary:
;; Base16: (https://github.com/chriskempson/base16)

;;; Authors: Quan Nguyen

;;; Code:

(require 'base16-theme)
(defvar base16-fantasy-colors
  '(
    :base00 "#343434"                 ;jet black
    :base01 "#848482"                 ;battleship grey
    :base02 "#6afb92"                 ;dragon green
    :base03 "#ffbf00"                 ;amber
    :base04 "#4169e1"                 ;royal blue
    :base05 "#7fffd4"                 ;aquamarine
    :base06 "#4b3621"                 ;cafe noir
    :base07 "#c0c0c0"                 ;silver
    :base08 "#dc143c"                 ;crimson
    :base09 "#e0115f"                 ;ruby
    :base0A "#d4af37"                 ;gold
    :base0B "#50c878"                 ;emerald
    :base0C "#86e0cc"                 ;mermaid green
    :base0D "#0f52ba"                 ;sapphire
    :base0E "#733635"                 ;garnet
    :base0F "#9966cc")                ;amethyst
  "All named color for Fantasy are defined here.")

;; Define the theme
(deftheme base16-fantasy)

;; Add all the faces to the theme
(base16-theme-define 'base16-fantasy base16-fantasy-colors)

;; Mark the theme as provided
(provide-theme 'base16-fantasy)

(provide 'base16-fantasy-theme)

;;; base16-fantasy-theme.el ends here
