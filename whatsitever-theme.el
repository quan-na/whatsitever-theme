;; whatsitever-theme.el -- A base16 colorscheme with only terminal named colors

;;; Commentary:
;; Base16: (https://github.com/chriskempson/base16)

;;; Authors: Quan Nguyen

;;; Code:

(require 'base16-theme)
(defvar whatsitever-colors
  '(:base00 "brightblack"                  ;0
    :base01 "black"                        ;8
    :base02 "brightgreen"                  ;10
    :base03 "brightyellow"                 ;11
    :base04 "brightblue"                   ;12
    :base05 "brightcyan"                   ;14
    :base06 "white"                        ;7
    :base07 "brightwhite"                  ;15
    :base08 "red"                          ;1
    :base09 "brightred"                    ;9
    :base0A "yellow"                       ;3
    :base0B "green"                        ;2
    :base0C "cyan"                         ;6
    :base0D "blue"                         ;4
    :base0E "brightmagenta"                ;13
    :base0F "magenta")                     ;5
  "All named color for Whatsitever are defined here.")

;; Define the theme
(deftheme whatsitever)

;; Add all the faces to the theme
(base16-theme-define 'whatsitever whatsitever-colors)

;; Mark the theme as provided
(provide-theme 'whatsitever)

(provide 'whatsitever-theme)

;;; whatsitever-theme.el ends here
